<?php

namespace App\Http\Controllers;

/**
 * @OA\Info(
 *     title="接口",
 *     version="1.0.0",
 *     description="接口"
 * )
 * @OA\SecurityScheme(
 *      securityScheme="bearerAuth",
 *      type="http",
 *      scheme="bearer",
 *      bearerFormat="JWT",
 *  )
 *
 */
abstract class Controller
{
    //
}
